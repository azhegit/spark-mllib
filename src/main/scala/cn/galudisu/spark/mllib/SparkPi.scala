package cn.galudisu.spark.mllib

import org.apache.spark.sql.SparkSession

import scala.math.random

/**
  * @author galudisu
  */
object SparkPi extends App {

  val spark = SparkSession
              .builder()
              .appName("FirstSparkApp")
              .config("spark.master", "spark://192.168.11.10:7077")
              .getOrCreate()
              .sparkContext

  var count = 0
  for (i <- 1 to 100000) {
    val x = random * 2 - 1
    val y = random * 2 - 1
    if (x * x + y * y < 1) count += 1
  }

  println("Pi is roughly " + 4.0 * count / 100000.0)


}
