package cn.galudisu.breeze

import breeze.linalg._
import breeze.numerics._
import breeze.stats._
import com.typesafe.scalalogging.LazyLogging
import org.scalatest.{BeforeAndAfter, FunSuite, Matchers}

import scala.io.Source
import scala.reflect.io.File
import scala.tools.nsc.classpath.FileUtils

/**
  *
  * @author galudisu
  */
class BreezeVizTests extends FunSuite with BeforeAndAfter with Matchers with LazyLogging {

  info("Starting...")

  /*------------
  |   变量部分
  ------------*/
  private var shrink: Double     = _
  private var ls    : List[Int]  = _
  private var as    : Array[Int] = _
  private var group : List[Int]  = _

  before {

    shrink = 0.8
    ls = List(1, 2, 3, 4, 5)
    as = Array(1, 2, 3, 4, 5)
    group = List(1, 2, 3, 4, 5, 9, 11, 20, 21, 22)
  }

  /**
    * Breeze-Viz 画图
    */
  test("Breeze-Viz highly experimental") {

    import breeze.linalg._
    import breeze.plot._

    val res = System.getProperty("user.dir") + "/res/"

    val f = Figure()
    val p = f.subplot(0)
    val x = linspace(0.0, 1.0)
    p += plot(x, x :^ 2.0)
    p += plot(x, x :^ 3.0, '.')
    p.xlabel = "x axis"
    p.ylabel = "y axis"
    f.saveas(res + "lines.png") // save current figure as a .png, eps and pdf also supported

    val p2 = f.subplot(2,1,1)
    val g = breeze.stats.distributions.Gaussian(0,1)
    p2 += hist(g.sample(100000),100)
    p2.title = "A normal distribution"
    f.saveas(res + "subplots.png")

    val f2 = Figure()
    f2.subplot(0) += image(DenseMatrix.rand(200,200))
    f2.saveas(res + "image.png")
  }
}

































