package cn.galudisu.breeze

import java.io.File

import breeze.linalg._
import breeze.numerics._
import breeze.stats._
import breeze.stats.distributions._
import com.typesafe.scalalogging.LazyLogging
import org.scalatest.{BeforeAndAfter, FunSuite, Matchers}

/**
  * 请事先安装下列本地库 ATLAS (http://math-atlas.sourceforge.net) and OpenBLAS (http://www.openblas.net/).
  * sudo apt-get install libatlas3-base libopenblas-base
  * sudo update-alternatives --config libblas.so.3
  * sudo update-alternatives --config liblapack.so.3
  *
  * Windows版，链接：http://pan.baidu.com/s/1qYawRqG 密码：0yoh
  *
  * @author galudisu
  */
class BreezeTests extends FunSuite with BeforeAndAfter with Matchers with LazyLogging {

  info("Starting...")

  /*------------
  |   变量部分
  ------------*/
  private var shrink: Double     = _
  private var ls    : List[Int]  = _
  private var as    : Array[Int] = _
  private var group : List[Int]  = _

  before {

    shrink = 0.8
    ls = List(1, 2, 3, 4, 5)
    as = Array(1, 2, 3, 4, 5)
    group = List(1, 2, 3, 4, 5, 9, 11, 20, 21, 22)
  }

  /**
    * chapter1
    */
  test("Getting Started with Breeze") {

    // dense vector
    val sparse = SparseVector(0.0, 1.0, 0.0, 2.0, 0.0)
    logger info s"$sparse"

    // zero vector
    val denseZeros = DenseVector.zeros[Double](5)
    logger info s"$denseZeros"

    // by function
    val denseTabulate = DenseVector.tabulate[Double](5)(index => index * index)
    logger info s"$denseTabulate"

    // a vector of linearly spaced values
    val spaceVector = linspace(2, 10, 5)
    logger info s"$spaceVector"

    // a vector with values in a specific range
    val allNosTill10 = DenseVector.range(0, 10)
    logger info s"$allNosTill10"
    val evenNosTill20 = DenseVector.range(0, 20, 2)
    logger info s"$evenNosTill20"
    val rangeD = DenseVector.rangeD(0.5, 20, 2.5)
    logger info s"$rangeD"

    // an entire vector with a single value
    val denseJust2s = DenseVector.fill(10, 2)
    logger info s"$denseJust2s"

    // slicing a sub-vector from a bigger vector
    val fourThroughSevenIndexVector = allNosTill10.slice(4, 7)
    logger info s"$fourThroughSevenIndexVector"
    val twoThroughNineSkip2IndexVector = allNosTill10.slice(2, 9, 2)
    logger info s"$twoThroughNineSkip2IndexVector"

    // from scala vector
    val vectFromArray = DenseVector(collection.immutable.Vector(1, 2, 3, 4))
    logger info s"$vectFromArray"

    // Vector arithmetic
    val inPlaceValueAddition = evenNosTill20 + 2
    logger info s"$inPlaceValueAddition"
    val inPlaceValueSubtraction = evenNosTill20 - 2
    logger info s"$inPlaceValueSubtraction"
    val inPlaceValueMultiplication = evenNosTill20 * 2
    logger info s"$inPlaceValueMultiplication"
    val inPlaceValueDivision = evenNosTill20 / 2
    logger info s"$inPlaceValueDivision"

    // Calculating the dot product of two vectors
    val justFive2s = DenseVector.fill(5, 2)
    val zeroThrough4 = DenseVector.range(0, 5, 1)
    val dotVector = zeroThrough4.dot(justFive2s)
    logger info s"$dotVector"

    // Creating a new vector by adding two vectors together
    val additionVector = evenNosTill20 + denseJust2s
    logger info s"$additionVector"

    val fiveLength = DenseVector(1, 2, 3, 4, 5)
    val tenLength = DenseVector.fill(10, 20)
    logger info s"${fiveLength + tenLength }" // 加数的长度必须≤ 被加数，否则抛出异常

    // Appending vectors and converting a vector of one type to another
    val concatVector = DenseVector.vertcat(zeroThrough4, justFive2s)
    logger info s"$concatVector"
    // 水平连接
    val concatVector1 = DenseVector.horzcat(zeroThrough4, justFive2s)
    logger info s"$concatVector1" // 垂直连接

    // Converting a vector of Int to a vector of Double
    val evenNosTill20Double = convert(evenNosTill20, Double)
    logger info s"$evenNosTill20Double"

    // Mean and variance 均值和方差
    logger info s"${meanAndVariance(evenNosTill20Double) }"

    // Standard deviation 标准差
    logger info s"${stddev(evenNosTill20Double) }"

    // Find the largest value in a vector
    val intMaxOfVectorVals = max(evenNosTill20)
    logger info s"$intMaxOfVectorVals"

    // Sum, Square root, log 求和、平方根、自然对数
    val intSumOfVectorVals = sum(evenNosTill20)
    logger info s"$intSumOfVectorVals"
    val sqrtOfVectorVals = sqrt(evenNosTill20)
    logger info s"$sqrtOfVectorVals"
    val log2VectorVals = log(evenNosTill20)
    logger info s"$log2VectorVals"
  }

  /**
    * 矩阵
    */
  test("Working with matrices") {

    // 密集矩阵
    val simpleMatrix = DenseMatrix((1, 2, 3), (11, 22, 33), (21, 22, 23))
    logger info s"$simpleMatrix"

    // 稀疏矩阵
    val sparseMatrix = CSCMatrix((1, 0, 0), (11, 0, 0), (0, 0, 23))
    logger info s"$sparseMatrix"

    // Creating a zero matrix
    val denseZeros = DenseMatrix.zeros[Double](5, 4)
    logger info s"$denseZeros"
    val compressedSparseMatrix = CSCMatrix.zeros[Double](5, 4)
    logger info s"$compressedSparseMatrix"

    // Creating a matrix out of a function
    val denseTabulate = DenseMatrix.tabulate(5, 4)((firstIdx, secondIdx) => firstIdx * secondIdx)
    logger info s"$denseTabulate"

    // Creating an identity matrix 单位矩阵
    val identityMatrix = DenseMatrix.eye[Int](3)
    logger info s"$identityMatrix"

    // Creating a matrix from random numbers
    val randomMatrix = DenseMatrix.rand(4, 4)
    logger info s"$randomMatrix"

    // Creating from a Scala collection
    val vectFromArray = new DenseMatrix(2, 2, Array(2, 3, 4, 5))
    logger info s"$vectFromArray"

    // Matrix arithmetic 矩阵运算
    val additionMatrix = identityMatrix + simpleMatrix      // 加法
    val simpleTimesIdentity = simpleMatrix * identityMatrix // 乘法
    val elementWiseMulti = identityMatrix :* simpleMatrix   // 单独操作
    logger info s"$additionMatrix"
    logger info s"$simpleTimesIdentity"
    logger info s"$elementWiseMulti"

    // Concatenating matrices – vertically 矩阵垂直连接
    val vertConcatMatrix=DenseMatrix.vertcat(identityMatrix, simpleMatrix)
    logger info s"$vertConcatMatrix"
    // Concatenating matrices – horizontally 水平连接
    val horzConcatMatrix=DenseMatrix.horzcat(identityMatrix, simpleMatrix)
    logger info s"$horzConcatMatrix"

    // Converting a matrix of Int to a matrix of Double
    val simpleMatrixAsDouble = convert(simpleMatrix, Double)
    logger info s"$simpleMatrixAsDouble"

    // Data manipulation operations 矩阵数据操作
    val simpleMatrix2 = DenseMatrix((4.0, 7.0), (3.0, -5.0))

    // Getting column vectors out of the matrix 从矩阵获取向量 - 列
    val firstVector = simpleMatrix2(::, 0)
    val secondVector = simpleMatrix(::, 1)
    val firstVectorByCols = simpleMatrix(0 to 1, 0)

    // Getting row vectors out of the matrix 从矩阵获取向量 - 行
    val firstRowStatingCols = simpleMatrix2(0, 0 to 1)
    val firstRowAllCols = simpleMatrix2(0, ::)
    val secondRow = simpleMatrix2(1, ::)

    // Getting values inside the matrix
    val firstRowFirstCol = simpleMatrix2(0, 0)
    logger info s"$firstRowFirstCol"

    // Getting the inverse and transpose of a matrix
    val simpleMatrix3 = DenseMatrix(
      ( 1.0,  1.0,  2.0),
      (-1.0,  2.0,  0.0),
      ( 1.0,  1.0,  3.0))
    val transpose = simpleMatrix3.t   // 转置
    val inverse=inv(simpleMatrix3)                          // 逆矩阵
    logger info s"${convert(simpleMatrix3 * inverse, Int)}" // 根据性质 AB=BA=E

    // Mean and variance 均值和方差
    logger info s"${meanAndVariance(convert(simpleMatrixAsDouble, Double))}"

    // Standard deviation 标准差
    logger info s"${stddev(simpleMatrixAsDouble)}"

    // Finding the largest value in a matrix
    logger info s"${max(simpleMatrix2)}"

    // Finding the sum, square root and log of all the values in the matrix
    logger info s"${sum(simpleMatrix2) }"

    // 平方根和自然对数
    logger info s"${sqrt(simpleMatrix2) }"
    logger info s"${log(simpleMatrix2) }"

    // Calculating the eigenvectors and eigenvalues of a matrix 计算矩阵的特征向量和特征值
    val denseEig = eig(simpleMatrix2)
    logger info s"$denseEig"
    val eigenVectors = denseEig.eigenvectors
    logger info s"$eigenVectors"
    val eigenValues = denseEig.eigenvalues
    logger info s"$eigenValues"
  }

  /**
    * 向量和矩阵随机分布值
    * 1. vectors with uniformly distributed random values               向量均匀分布随机值
    * 2. vectors with normally distributed random values                向量正态分布随机值
    * 3. vectors with random values that have a Poisson distribution    向量泊松分布的随机值
    * 4. matrix with uniformly random values                            矩阵均匀随机值
    * 5. matrix with normally distributed random values                 矩阵正态分布随机值
    * 6. matrix with random values that has a Poisson distribution      矩阵泊松分布的随机值
    */
  test("Vectors and matrices with randomly distributed values") {

    //Uniform distribution with low being 0 and high being 10
    val uniformDist = Uniform(0, 10)  // 均匀分布
    //Gaussian distribution with mean being 5 and Standard deviation being 1
    val gaussianDist = Gaussian(5, 1) // 高斯分布(正态分布)
    //Poission distribution with mean being 5
    val poissonDist = Poisson(5)      // 泊松分布

    //Samples a single value
    logger info s"${uniformDist.sample() }"
    //eg. 9.151191360491392
    //Returns a sample vector of size that is passed in as parameter
    logger info s"${uniformDist.sample(2) }"
    //eg. Vector(6.001980062275654, 6.210874664967401)

    /*===============================================
     |               向量随机分布
     ===============================================*/

    // Creating vectors with uniformly distributed random values - 均匀分布
    val uniformWithoutSize = DenseVector.rand(10)
    logger info s"$uniformWithoutSize"

    val uniformVectInRange = DenseVector.rand(10, uniformDist)
    logger info s"$uniformVectInRange"

    // Creating vectors with normally distributed random values - 高斯分布
    val gaussianVector = DenseVector.rand(10, gaussianDist)
    logger info s"$gaussianVector"

    // Creating vectors with random values that have a Poisson distribution - 泊松分布
    val poissonVector = DenseVector.rand(10, poissonDist)
    logger info s"$poissonVector"

    /*===============================================
    |                矩阵随机分布
    ===============================================*/

    val uniformMat = DenseMatrix.rand(3, 3)
    logger info s"$uniformMat"
    //Creates a 3 * 3 Matrix with uniformly distributed random values with low being 0 and high being 10
    val uniformMatrixInRange = DenseMatrix.rand(3, 3, uniformDist)
    logger info s"$uniformMatrixInRange"

    // Creating a matrix with normally distributed random values - 高斯分布
    val gaussianMatrix = DenseMatrix.rand(3, 3, gaussianDist)
    logger info s"$gaussianMatrix"

    // Creating a matrix with random values that has a Poisson distribution - 泊松分布
    val poissonMatrix = DenseMatrix.rand(3, 3, poissonDist)
    logger info s"$poissonMatrix"

  }

  /**
    * 覆盖有
    * 1. Read a CSV file into a matrix
    * 2. Save selected columns of a matrix into a new matrix
    * 3. Write the newly created matrix into a CSV file
    * 4. Extract a vector out of the matrix
    * 5. Write the vector into a CSV
    */
  test("Reading and writing CSV files") {
    // Use `csvread` function to read a CSV file into a 100*3 matrix
    val usageMatrix = csvread(file = new File("csv/WWWusage.csv"), separator = ',', skipLines = 1)
    //print first five rows
    logger info s"${usageMatrix(0 to 5, ::) }"

    // Extra a sub-matrix out of the read matrix
    val firstColumnSkipped = usageMatrix(::, 1 until usageMatrix.cols)
    logger info s"${firstColumnSkipped(0 to 5, ::) }"

    // Write this modified matrix to a file
    csvwrite(file = new File("csv/firstColumnSkipped.csv"), mat = firstColumnSkipped, separator = ',')
  }

}

































