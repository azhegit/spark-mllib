package cn.galudisu.spark._1_dataframes

import com.typesafe.scalalogging.LazyLogging
import org.scalatest.{BeforeAndAfter, FunSuite, Matchers}

/**
  * 1. Getting Apache Spark
  * 2. Creating a DataFrame from CSV
  * 3. Manipulating DataFrames
  * 4. Creating a DataFrame from Scala case classes
  *
  * @author galudisu
  */
class DataFramesTests extends FunSuite with BeforeAndAfter with Matchers with LazyLogging {

  info("Starting...")

  /*------------
  |   变量部分
  ------------*/
  private var shrink: Double     = _
  private var ls    : List[Int]  = _
  private var as    : Array[Int] = _
  private var group : List[Int]  = _

  before {

    shrink = 0.8
    ls = List(1, 2, 3, 4, 5)
    as = Array(1, 2, 3, 4, 5)
    group = List(1, 2, 3, 4, 5, 9, 11, 20, 21, 22)
  }

  // 由于Spark中
}
