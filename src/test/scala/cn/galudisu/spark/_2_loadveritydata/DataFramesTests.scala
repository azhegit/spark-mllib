package cn.galudisu.spark._2_loadveritydata

import com.typesafe.scalalogging.LazyLogging
import org.scalatest.{BeforeAndAfter, FunSuite, Matchers}

/**
  * 1. Loading more than 22 features into classes
  * 2. Loading JSON into DataFrames
  * 3. Storing data as Parquet files
  * 4. Using the Avro data model in Parquet
  * 5. Loading from RDBMS
  * 6. Preparing data in DataFrames  *
  *
  * @author galudisu
  */
class DataFramesTests extends FunSuite with BeforeAndAfter with Matchers with LazyLogging {

  info("Starting...")

  /*------------
  |   变量部分
  ------------*/
  private var shrink: Double     = _
  private var ls    : List[Int]  = _
  private var as    : Array[Int] = _
  private var group : List[Int]  = _

  before {

    shrink = 0.8
    ls = List(1, 2, 3, 4, 5)
    as = Array(1, 2, 3, 4, 5)
    group = List(1, 2, 3, 4, 5, 9, 11, 20, 21, 22)

  }

  test("Loading more than 22 features into classes") {

  }
}
