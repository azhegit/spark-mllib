name := "spark-mllib"

version := "1.0"

scalaVersion := "2.11.8"

organization := "Spark Machine Learning"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "2.1.0",
  "org.apache.spark" %% "spark-sql" % "2.1.0",
  "org.apache.spark" %% "spark-mllib" % "2.1.0",
  "com.databricks" %% "spark-csv" % "1.5.0",
  "com.typesafe.akka" %% "akka-actor" % "2.4.12",
  "com.typesafe.slick" %% "slick" % "3.1.1",
  "org.scalanlp" %% "breeze" % "0.12",
  "org.scalanlp" %% "breeze-natives" % "0.12",
  "org.scalanlp" %% "breeze-viz" % "0.12",
  "org.scalaz" %% "scalaz-core" % "7.2.8",
  "mysql" % "mysql-connector-java" % "5.1.25",
  "org.scalatest" % "scalatest_2.11" % "2.2.6",
  "junit" % "junit" % "4.12",
  "org.apache.commons" % "commons-lang3" % "3.5",
  "org.hamcrest" % "hamcrest-all" % "1.3",
  "com.google.guava" % "guava" % "14.0.1",
  "org.mockito" % "mockito-all" % "1.9.5",
  "org.slf4j" % "slf4j-api" % "1.7.22",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0",
  "org.apache.avro" % "avro" % "1.8.1",
  "org.apache.parquet" % "parquet-avro" % "1.9.0",
  "com.twitter" %% "chill-avro" % "0.6.0"

)

resolvers ++= Seq(
  "Apache HBase" at "https://repository.apache.org/content/repositories/releases",
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/",
  "Twitter" at "http://maven.twttr.com/",
  "Spray repository" at "http://repo.spray.io",
  "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases",
  "releases" at "http://dl.bintray.com/sbt/sbt-plugin-releases/",
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/",
  "Typesafe Snapshots Repository" at "http://repo.typesafe.com/typesafe/snapshots/",
  "Typesafe Snapshots Maven Repository" at "http://repo.typesafe.com/typesafe/maven-releases/"
)

fork := true
//seq(sbtavro.SbtAvro.avroSettings: _*)
//(stringType in avroConfig) := "String"
//javaSource in sbtavro.SbtAvro.avroConfig <<= (sourceDirectory in
//  Compile) (_ / "java")

unmanagedSourceDirectories in Compile <<= baseDirectory { base =>
  Seq(
    base / "src/main/scala"
  )
}
scalacOptions ++= Seq("-deprecation", "-unchecked", "-feature")